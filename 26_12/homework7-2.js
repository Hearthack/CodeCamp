const fs = require("fs");

let readFileAndJSONParse = filename => {
	return new Promise((resolve, reject) => {
		fs.readFile(filename, "utf8", (err, data) => {
			if (err) reject(err);
			else
				//console.log("Read file" + " " + filename + " " + "complete.");
				resolve(JSON.parse(data));
		});
	});
};

let functionaddYearSalary = row => {
	let yearSalary = (parseInt(row.salary) * 12).toString();
	return { ...row, yearSalary };
};

// Object.assign({}, row, {NextSalary})
let addNextSalary = row => {
	let salary = parseInt(row.salary);
	let NextSalary = [salary, salary * 1.1, salary * 1.21];
	return { ...row, NextSalary };
};

function addAdditionalFields(data) {
	return data.map(functionaddYearSalary).map(addNextSalary);
}

let cloneArrayObj = data => {
	return new Promise((resolve, reject) => {
		try {
			let newCopy = [],
				newRow;
			data.map(row => {
				newRow = {};
				Object.keys(row).map(key => {
					newRow[key] = row[key];
				});
				newCopy.push(newRow);
			});
			resolve(newCopy);
		} catch (error) {
			reject(error);
		}
	});
};

async function runMyFunction() {
	try {
		let employees = await readFileAndJSONParse("homework1.json");
		let newEmployees = addAdditionalFields(employees);
		newEmployees[0].salary = 0;
		console.log(employees);
		console.log(newEmployees);
		console.log(
			`employess.salary = ${employees[0].salary} \nnewEmployees.salary = ${
				newEmployees[0].salary
			}`
		);
	} catch (error) {
		console.error(error);
	}
}
runMyFunction();
