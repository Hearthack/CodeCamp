const fs = require("fs");

let readFileAndJSONParse = filename => {
	return new Promise((resolve, reject) => {
		fs.readFile(filename, "utf8", (err, data) => {
			if (err) reject(err);
			else
				//console.log("Read file" + " " + filename + " " + "complete.");
				resolve(JSON.parse(data));
		});
	});
};

let functionaddYearSalary = row => {
	row.yearSalary = (parseInt(row.salary) * 12).toString();
};

let addNextSalary = row => {
	let salary = parseInt(row.salary);
	row.NextSalary = [salary, salary * 1.1, salary * 1.21];
};

let addAdditionalFields = data => {
	data.map(x => {
		functionaddYearSalary(x);
		addNextSalary(x);
	});
	console.log(data);
};

async function runMyFunction() {
	try {
		let employees = await readFileAndJSONParse("homework1.json");
		await addAdditionalFields(employees);
	} catch (error) {
		console.error(error);
	}
}
runMyFunction();
