let cloneArray = arr => {
	let newPointer, arrayFirst;
	newPointer = [];
	for (let first in arr) {
		newPointer.push(arr[first]);
	}
	return newPointer;
};

let checkArrayOrObject = dataIn => {
	let dataType = "";
	if (!!dataIn) {
		if (dataIn.constructor === Array) {
			dataType = "Array";
		}
		if (dataIn.constructor === Object) {
			dataType = "Object";
		}
	}
	return dataType;
};

let cloneObjectOrArray = pointer => {
	return new Promise((resolve, reject) => {
		try {
			let newPointer, arrayFirst, newData;

			if (checkArrayOrObject(pointer) == "Array") {
				newPointer = [];
				pointer.map(data => {
					if (checkArrayOrObject(data) == "Array") {
						let newData22 = [];
						data.map(newdata2 => {
							if (checkArrayOrObject(newdata2) == "Array") {
								let newData33 = [];
								newdata2.map(newdata3 => {
									newData33.push(newdata3);
								});
								newData22.push(newData33);
							} else if (checkArrayOrObject(newdata2) == "Object") {
								let newData4 = {};
								Object.keys(newdata2).map(keyData2 => {
									newData4[keyData2] = newdata2[keyData2];
								});
								newData22.push(newData4);
							} else {
								newData22.push(newdata2);
							}
						});
						newPointer.push(newData22);
					} else if (checkArrayOrObject(data) == "Object") {
						newData = {};
						Object.keys(data).map(keyData => {
							if (checkArrayOrObject(data[keyData]) == "Object") {
								let newData2 = {};
								Object.keys(data[keyData]).map(keyData2 => {
									newData2[keyData2] = data[keyData][keyData2];
								});
								newData[keyData] = newData2;
							} else if (checkArrayOrObject(data[keyData]) == "Array") {
								newData[keyData] = data[keyData];
							} else {
								newData[keyData] = data[keyData];
							}
						});
						newPointer.push(newData);
					} else {
						newPointer.push(data);
					}
				});
			} else if (checkArrayOrObject(pointer) == "Object") {
				newPointer = {};
				Object.keys(pointer).map(keyData => {
					if (checkArrayOrObject(pointer[keyData]) == "Object") {
						newData2 = {};
						Object.keys(pointer[keyData]).map(keyData4 => {
							//newData2[keyData2] = pointer[keyData][keyData2];
							if (checkArrayOrObject(pointer[keyData][keyData4]) == "Object") {
								let newData3 = {};
								Object.keys(pointer[keyData][keyData4]).map(keyData3 => {
									newData3[keyData3] = pointer[keyData][keyData4][keyData3];
								});
								newData2[keyData] = newData3;
							} else if (
								checkArrayOrObject(pointer[keyData][keyData4]) == "Array"
							) {
								let newData33 = [];
								pointer[keyData][keyData4].map(newdata3 => {
									newData33.push(newdata3);
								});
								newData2[keyData4] = newData33;
							} else {
								newData2[keyData4] = pointer[keyData][keyData4];
							}
						});
						newPointer[keyData] = newData2;
					} else if (checkArrayOrObject(pointer[keyData]) == "Array") {
						let newData22 = [];
						pointer[keyData].map(newdata2 => {
							if (checkArrayOrObject(newdata2) == "Array") {
								let newData33 = [];
								newdata2.map(newdata3 => {
									newData33.push(newdata3);
								});
								newData22.push(newData33);
							} else if (checkArrayOrObject(newdata2) == "Object") {
								let newData4 = {};
								Object.keys(newdata2).map(keyData2 => {
									newData4[keyData2] = newdata2[keyData2];
								});
								newData22.push(newData4);
							} else {
								newData22.push(newdata2);
							}
						});
						newPointer[keyData] = newData22;
					} else {
						newPointer[keyData] = pointer[keyData];
					}
				});
			} else {
				newPointer = pointer;
			}

			resolve(newPointer);
		} catch (err) {
			reject(err);
		}
	});
};

async function runMyFunction() {
	try {
		let ObjOrArr = { a: 1, d: { z: 3, i: 4, k: [2, 5] }, c: 3 }; //[1, 2, { a: 1, b: { c: 3, d: 4 } }]; //{ a: 1, b: 2 }; [1, 2, { a: 1, b: 2 }]; //[1, 2, 3];
		let newObjOrArr = await cloneObjectOrArray(ObjOrArr);
		//newObjOrArr[2].b.c = 0;
		//newObjOrArr["b"] = 6;
		console.log(ObjOrArr);
		console.log(newObjOrArr);
	} catch (error) {
		console.log(error);
	}
}
runMyFunction();
exports.cloneObjectOrArray = cloneObjectOrArray;
