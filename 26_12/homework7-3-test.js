const assert = require("assert");
const fs = require("fs");

const myFunction = require("./homework7-3.js");
const cloneObjectOrArray = myFunction.cloneObjectOrArray;

describe("tdd lab", () => {
	describe("#cloneObjectOrArray()", function() {
		describe("#objectValue()", () => {
			it("should have same value in Array or Object.", async () => {
				let case1 = [1, 2, 3];
				let cloneCase1 = await cloneObjectOrArray(case1);
				assert.deepEqual(case1, cloneCase1, "Array or Value is not same");
			});

			it("should have same value in Array or Object.", async () => {
				let case2 = { a: 1, b: 2 };
				let cloneCase2 = await cloneObjectOrArray(case2);
				assert.deepEqual(case2, cloneCase2, "Array or Value is not same");
			});

			it("should have same value in Array or Object.", async () => {
				let case3 = [1, 2, { a: 1, b: 2 }];
				let cloneCase3 = await cloneObjectOrArray(case3);
				assert.deepEqual(case3, cloneCase3, "Array or Value is not same");
			});

			it("should have same value in Array or Object.", async () => {
				let case4 = [1, 2, { a: 1, b: { c: 3, d: 4 } }];
				let cloneCase4 = await cloneObjectOrArray(case4);
				assert.deepEqual(case4, cloneCase4, "Array or Value is not same");
			});
			it("should have not same value in Array or Object.", async () => {
				let case1 = [1, 2, 3];
				let cloneCase1 = await cloneObjectOrArray(case1);
				cloneCase1[0] = 0;
				assert.notDeepEqual(case1, cloneCase1, "Array or Object is not clone");
			});

			it("should have not same value in Array or Object.", async () => {
				let case2 = { a: 1, b: 2 };
				let cloneCase2 = await cloneObjectOrArray(case2);
				cloneCase2["a"] = 0;
				assert.notDeepEqual(case2, cloneCase2, "Array or Object is not clone");
			});

			it("should have not same value in Array or Object.", async () => {
				let case3 = [1, 2, { a: 1, b: 2 }];
				let cloneCase3 = await cloneObjectOrArray(case3);
				cloneCase3[2]["a"] = 0;
				assert.notDeepEqual(case3, cloneCase3, "Array or Object is not clone");
			});

			it("should have not same value in Array or Object.", async () => {
				let case4 = [1, 2, { a: 1, b: { c: 3, d: 4 } }];
				let cloneCase4 = await cloneObjectOrArray(case4);
				cloneCase4[2]["b"]["c"] = 0;
				assert.notDeepEqual(case4, cloneCase4, "Array or Object is not clone");
			});
		});
	});
});
