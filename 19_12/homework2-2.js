let fields = ['id','firstname','lastname','company','salary'];
let employees = [
    ['1001','Luke','Skywalker','Walt Disney','40000'],
    ['1002','Tony','Stark','Marvel','1000000'],
    ['1003','Somchai','Jaidee','Love2work','20000'],
    ['1004','Monkey D','Luffee','One Piece','9000000']
];
let employeesTable =[];
for(let i=0;i<employees.length;i++){
    let obj = {};
    if (employees[i].length == fields.length){
        for (let j=0;j<fields.length;j++){
            obj[fields[j]] = employees[i][j];
        }
    }
    employeesTable.push(obj);
    
}
console.log(employeesTable);