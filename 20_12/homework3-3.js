const fs = require("fs");

let employees = [
	{ id: "1001", firstname: "Luke", lastname: "Skywalker" },
	{ id: "1002", firstname: "Tony", lastname: "Stark" },
	{ id: "1003", firstname: "Somchai", lastname: "Jaidee" },
	{ id: "1004", firstname: "Monkey D", lastname: "Luffee" }
];
let company = [
	{ id: "1001", company: "Walt Disney" },
	{ id: "1002", company: "Marvel" },
	{ id: "1003", company: "Love2work" },
	{ id: "1004", company: "One Piece" }
];
let salary = [
	{ id: "1001", salary: "40000" },
	{ id: "1002", salary: "1000000" },
	{ id: "1003", salary: "20000" },
	{ id: "1004", salary: "9000000" }
];
let like = [
	{ id: "1001", like: "apple" },
	{ id: "1002", like: "banana" },
	{ id: "1003", like: "orange" },
	{ id: "1004", like: "papaya" }
];
let dislike = [
	{ id: "1001", dislike: "banana" },
	{ id: "1002", dislike: "orange" },
	{ id: "1003", dislike: "papaya" },
	{ id: "1004", dislike: "apple" }
];

function getEmployees() {
	return new Promise(function(resolve, reject) {
		let employeesDatabase = [];

		for (let i = 0; i < employees.length; i++) {
			let obj = {};
			for (let key in employees[i]) {
				obj[key] = employees[i][key];
			}
			for (let key in company[i]) {
				obj[key] = company[i][key];
			}
			for (let key in salary[i]) {
				obj[key] = salary[i][key];
			}
			for (let key in like[i]) {
				obj[key] = like[i][key];
			}
			for (let key in dislike[i]) {
				obj[key] = dislike[i][key];
			}
			//console.log(obj);
			employeesDatabase.push(obj);
		}
		resolve(JSON.stringify(employeesDatabase));
	});
}

function writeFile(filenameTxt, textIn) {
	return new Promise(function(resolve, reject) {
		fs.writeFile(filenameTxt, textIn, "utf8", function(err) {
			if (!err) {
				console.log("Write file " + filenameTxt + " Complete.");
			} else {
				reject(err);
			}
		});
	});
}

async function writeFileEmployeesDatabase() {
	try {
		let employeesData = await getEmployees();
		await writeFile("homework3-3.json", employeesData);
	} catch (error) {
		console.error(error);
	}
}

writeFileEmployeesDatabase();

/*
//Secound way.
function mergeData(employee, dataArr) {
	for (let i in DataArr) {
		for (let j in DataArr[i]) {
			if (j != "id" && employee.id == dataArr[i].id) {
				employee[j] = dataArr[i][j];
				break;
			}
		}
	}
}

employeeDatabase = [];
for (let i = 0; i < employees.length; i++) {
	employeeDatabase[i] = employees[i];
	mergeData(employeeDatabase[i], company);
	mergeData(employeeDatabase[i], salary);
	mergeData(employeeDatabase[i], like);
	mergeData(employeeDatabase[i], dislike);
}
console.log(employeeDatabase);
*/
