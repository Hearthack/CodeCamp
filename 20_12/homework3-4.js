const fs = require("fs");

function readTextToJSON(filenameTxt) {
	return new Promise(function(resolve, reject) {
		fs.readFile(filenameTxt, "utf8", function(err, dataTxt) {
			if (err) {
				reject(err);
				console.log(err);
			} else {
				resolve(JSON.parse(dataTxt));
			}
		});
	});
}

function createObj(employeesData) {
	console.log("in function Obj.");
	//console.log(employeesData);
	let i = 0;
	let employeesTable = [];

	employeesData.forEach(function(data) {
		//console.log("row" + i + ">>>" + data)
		let Obj = {};
		Object.keys(data).forEach(function(data1) {
			if (data1 == "name") {
				Obj[data1] = data[data1];
			}
			if (data1 == "gender") {
				Obj[data1] = data[data1];
			}
			if (data1 == "company") {
				Obj[data1] = data[data1];
			}
			if (data1 == "email") {
				Obj[data1] = data[data1];
			}
			if (data1 == "friends") {
				Obj[data1] = data[data1];
			}
		});
		//console.log(Obj);
		employeesTable.push(Obj);
	});
	console.log(employeesTable);
}

async function writeFileEmployeesDatabase() {
	try {
		let employeesData = await readTextToJSON("homework1-4.json");
		await createObj(employeesData);
	} catch (error) {
		console.error(error);
	}
}

writeFileEmployeesDatabase();

//Secound way
/*
let newPeople = [];
let chooseArray = ["name", "gender", "company", "email", "friends"];
people.forEach(person => {
	let objRow = {};
	chooseArray.forEach(key => {
		objRow[key] = person[key];
    });
    newPeople.push(objRow);
});
*/
