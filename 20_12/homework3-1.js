const fs = require("fs");

function readFile(filenameTxt) { 
    return new Promise(function(resolve, reject) {
        fs.readFile(filenameTxt, 'utf8', function(err, dataTxt) { 
             if (err) 
                reject(err); 
            else 
                resolve(dataTxt); 
        }); 
    }); 
}

let head = new Promise(function (resolve, reject) {
    resolve(readFile("head.txt")); 
}); 
let body = new Promise(function (resolve, reject) { 
    resolve(readFile("body.txt")); 
}); 
let leg = new Promise(function (resolve, reject) {
    resolve(readFile("leg.txt")); 
});
let feet = new Promise(function (resolve, reject) {
    resolve(readFile("feet.txt")); 
});

Promise.all([head, body, leg, feet]) 
.then(function(result){
    console.log('All completed!: ', result); // result = ['one','two','three']
    fs.writeFile('Robot.txt',result.join("\r\n"),'utf8', function(err){
        if (!err) {
            console.log("Write Robot.txt Complete.")
        }else{
            console.log(err);
        }
    }); 
}) 
.catch(function(error){ 
    console.error("There's an error", error); 
});
