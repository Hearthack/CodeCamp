const fs = require("fs");

function readFile(filenameTxt) { 
    return new Promise(function(resolve, reject) {
        fs.readFile(filenameTxt, 'utf8', function(err, dataTxt) { 
            if (err) 
                reject(err); 
            else 
                resolve(dataTxt); 
        }); 
    }); 
}

function writeFile(filenameTxt,textIn) { 
    return new Promise(function(resolve, reject) {
        fs.writeFile(filenameTxt,textIn, 'utf8', function(err) { 
            if (!err) {
                console.log("Write file Robot.txt by Async/Await Complete.")
            }else{
                reject(err); 
            }
        }); 
    }); 
}

async function writeFileRobot() { 
    try {
        let head = await readFile("head.txt"); 
        let body = await readFile("body.txt"); 
        let leg  = await readFile("leg.txt");
        let feet = await readFile("feet.txt");
        let result = [head,body,leg,feet].join("\r\n");
        await writeFile("Robot.txt",result);
    } catch (error) {
        console.error(error); 
    } 
} 

writeFileRobot();