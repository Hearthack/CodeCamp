const fs = require("fs");

function readTextToJSON(filenameTxt) { 
    return new Promise(function(resolve, reject) {
        fs.readFile(filenameTxt, 'utf8', function(err, dataTxt) { 
            if (err) { 
                reject(err);
                console.log(err);
            }else{
                resolve(JSON.parse(dataTxt));
            } 
                

        }); 
    }); 
}

function Lab(dataObj) { 
    return new Promise(function(resolve, reject) {
        
        let y = dataObj.filter(x => {
            let condition1 = x.gender == "male";
            let condition2 = x.friends.length >= 2;
            return  condition1 && condition2;
        })
        .map( z => {
            let myObj = {};
            myObj.name = z.name;
            myObj.gender = z.gender;
            myObj.company = z.company;
            myObj.email = z.email;
            myObj.friends = z.friends;
            myObj.balance = "$" + z.balance.substr(1).replace(",","")/10;
            return myObj;
        });
        resolve(y);

         
    }); 
}

function Lab2(dataObj) { 
    return new Promise(function(resolve, reject) {
        let y = dataObj.map(x => {
            x.balance = "$" + x.balance.substr(1).replace(",","")/10;            
            return x;
        });
        resolve(y);         
    }); 
}

async function writeFileEmployeesDatabase() { 
    try {
        let employeesData = await readTextToJSON("homework1-4.json");
        //console.log(employeesData); 
        let result = await Lab(employeesData);
        console.log(result);
        let result2 = await Lab2(employeesData);
        //console.log(result2);
    } catch (error) {
        console.error(error); 
    } 
} 

writeFileEmployeesDatabase();