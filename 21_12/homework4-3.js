const fs = require("fs");

function readTextToJSON(filenameTxt) { 
    return new Promise(function(resolve, reject) {
        fs.readFile(filenameTxt, 'utf8', function(err, dataTxt) { 
            if (err) { 
                reject(err);
                console.log(err);
            }else{
                resolve(JSON.parse(dataTxt));
            } 
                

        }); 
    }); 
}

function Lab(dataObj) { 
    return new Promise(function(resolve, reject) {
        
        let y = dataObj.map(x => {
            if (x.salary < 1e5) {
                x.salary = x.salary*2;
            }
            return x;
        });
        resolve(y);

         
    }); 
}

function Lab2(dataObj) { 
    return new Promise(function(resolve, reject) {
        let sum = 0;
        // let y = dataObj.map(x => {
        //     sum += parseInt(x.salary);            
        //     return x;
        // });
        sum = dataObj.reduce((total,data) => {
            return parseInt(total) + parseInt(data.salary);
        },0);
        resolve(sum);

         
    }); 
}

async function writeFileEmployeesDatabase() { 
    try {
        let employeesData = await readTextToJSON("homework1.json");
        //console.log(employeesData); 
        let result = await Lab(employeesData);
        console.log(result);
        let result2 = await Lab2(result);
        console.log(result2);
    } catch (error) {
        console.error(error); 
    } 
} 

writeFileEmployeesDatabase();