const fs = require("fs");

function readTextToJSON(filenameTxt) { 
    return new Promise(function(resolve, reject) {
        fs.readFile(filenameTxt, 'utf8', function(err, dataTxt) { 
            if (err) { 
                reject(err);
                console.log(err);
            }else{
                resolve(JSON.parse(dataTxt));
            } 
                

        }); 
    }); 
}

function Lab(dataObj) { 
    return new Promise(function(resolve, reject) {
        
        let y = dataObj.map(x => {
            x.fullname = x.firstname + " " +  x.lastname;
            let salary2 = (x.salary*110)/100;
            let salary3 = (salary2*110)/100;
            x.salary = [x.salary,salary2,salary3];
            return x;
        });
        resolve(y);

         
    }); 
}

async function writeFileEmployeesDatabase() { 
    try {
        let employeesData = await readTextToJSON("homework1.json");
        //console.log(employeesData); 
        let result = await Lab(employeesData);
        console.log(result);

    } catch (error) {
        console.error(error); 
    } 
} 

writeFileEmployeesDatabase();